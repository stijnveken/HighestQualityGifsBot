# HighestQualityGifsBot

This bot can be used to scan one subreddit and post the submission to an other and giving credit to the OP.

# Setup

* Clone the project using *git clone https://github.com/thefakegm/HighestQualityGifsBot.git*

* install the requirements with *sudo pip install -r requirements.txt*

* Make a file called settings.py with the following contents:

> This file contains secrets that should not be shared. Never EVER commit this to git.

>USER_AGENT = 'USER_AGENT'

>SCOPES = ['identity', 'submit', 'read', 'privatemessages', 'modposts']

>SUBREDDIT_SCAN = 'SUBREDDIT_TO_SCAN'

>SUBREDDIT_POST = 'SUBREDDIT_TO_POST_TO'

>SUBJECT = 'CREDIT_MESSAGE_SUBJECT'

>MESSAGE = 'CREDIT_MESSAGE_MESSAGE'

>CREDIT = 'CREDIT_COMMENT'

>LOGGING_SUCCESS = 'LOGGING_MESSAGE'

>SUCCESS_OP = 'USER_TO_MESSAGE_ON_SUCCESS'

>SUCCESS_SUBJECT = 'THE_SUBJECT_OF_THE_SUCCESS_MESSAGE'

>SUCCESS_MESSAGE = 'THE_MESSAGE_OF_THE_SUCCESS_MESSAGE'

> # SECRETS, KEEP SECRET

>APP_SECRET = ''

>APP_KEY = ''

>ACCESS_TOKEN = ''

>REFRESH_TOKEN = ''

* Run onetime.py and give the bot access to the account you want to use. This will generate the Acces token and the Refresh token.

* That's it, you're good to go. Run the bot with *python /path/to/bot/bot.py*

